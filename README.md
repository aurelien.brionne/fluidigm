<img src="./inst/extdata/univ.png" align="right"/>
<img src="./inst/extdata/boa.png" align="right"/>
<img src="./inst/extdata/inra.png" align="right"/>

# fluidigm R package

Fluidigm QPCR normalisation and statistical analysis.

## Installation

```r
## using install_gitlab
remotes::install_gitlab(
    "aurelien.brionne/fluidigm",
    host = "forgemia.inra.fr",
    build_opts = c("--no-resave-data", "--no-manual")
)

## alternative (from local directory)
    # clone package (from prompt)
    git clone https://forgemia.inra.fr/aurelien.brionne/fluidigm.git

    # build package (from R console) 
    devtools::build("fluidigm")

    # install package (from R console)
    install.packages("fluidigm_1.0.tar.gz", repos = NULL, type = "source")
```







```

## Quick overview    
 
we show how use `fluidigm` R package to find the most stable reference/housekeeping genes using geNorm algorithm, normalize values and compute statistics (linear model).
 
1. Load and parse fluidigm file using `fluidigm::read` function.
 
2. Select the most stable reference/housekeeping genes with `fluidigm::geNorm` function.

![](./inst/extdata/M.png) 

![](./inst/extdata/V.png) 

3. Normalize target genes values using `fluidigm::Normalize` function.
 
4. Compute statistics using  `eggbiomechanic::var_test` function.

* using linear model

![](./inst/extdata/lm.png) 

* using linear mixt model

![](./inst/extdata/lme.png)
